/*Activity

	>> Create sample documents following the models we have created for users and courses for our booking-system.

		>> user1 and user2 are the ids for the both user documents.
		>> course1 and course2 are the ids for both course documents.

		>> user1 is enrolled in course1 and course2.
		>> user2 is enrolled in course2.

	>> Create an activity folder under s27
	>> Create a users.json file to store your user documents
	>> Create courses.json file to store your courses documents

users
	{
		id - unique
		firstName
		lastName
		email
		password
		isAdmin: boolean
		enrollmentDetails: [

			{
				id - unique - userEnroll1
				course_id
				dateEnrolled
				status
			}

		]
	}

courses
	{
		id - unique
		name
		description
		price
		enrollmentDetails: [

			{
				id - unique - enrollUser1
				user_id
				dateEnrolled
				status
			}

		]
	}
*/


Courses

id 		name 												price
1 		Full Stack Web Developer Program - FT 				50000
2 		Full Stack Web Developer Program - PT				75000


Users
id 		name
1. 		Lester Alarcon
2.		Juan Logic
3.		John Lann




